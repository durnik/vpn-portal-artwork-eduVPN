<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

return [
    'Android' => 'Android',
    'If you do not want to, or cannot use the official eduVPN apps, you can also manually obtain a VPN configuration and import it in your existing VPN application.' => 'Pokud si nepřejete nebo nemůžete používat oficiální aplikace eduVPN, můžete si vygenerovat a stáhnout VPN konfiguračný soubor a importovat ji do vaší stávající VPN aplikace.',
    'Linux' => 'Linux',
    'Manual Configuration' => 'Manuální konfigurace',
    'On the "Account" page you can block access to the VPN in case you lose a device, or no longer use the VPN.' => 'Na stránce "Účet" můžete zablokovat přístup k VPN v případě, že ztratíte zařízení nebo již VPN nebudete používát.',
    'To use eduVPN, download the app for your device below!' => 'Pro použití eduVPN stáhněte aplikaci pro své zařízení níže!',
    'Welcome to eduVPN!' => 'Vítejte v eduVPN!',
    'Windows' => 'Windows',
    'iOS' => 'iOS',
    'macOS' => 'macOS',
];
