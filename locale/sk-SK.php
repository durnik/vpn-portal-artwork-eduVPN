<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

return [
    'Android' => 'Android',
    'If you do not want to, or cannot use the official eduVPN apps, you can also manually obtain a VPN configuration and import it in your existing VPN application.' => 'Ak si neželáte alebo nemôžete použiť oficiálne eduVPN aplikácie, môžete si vygenerovat a stiahnuť VPN konfiguračný súbor a importovať ho do Vašej existujúcej VPN aplikácie.',
    'Linux' => 'Linux',
    'Manual Configuration' => 'Manuálna konfigurácia',
    'On the "Account" page you can block access to the VPN in case you lose a device, or no longer use the VPN.' => 'Na stránke "Účet" môžete zablokovať prístup k VPN v prípade, že ste stratili Vaše zariadenie alebo VPN už nebudete používať.',
    'To use eduVPN, download the app for your device below!' => 'Aby ste mohli používať eduVPN, stiahnite si aplikáciu pre svoje zariadenie nižšie!',
    'Welcome to eduVPN!' => 'Vitajte v eduVPN!',
    'Windows' => 'Windows',
    'iOS' => 'iOS',
    'macOS' => 'macOS',
];
