<?php declare(strict_types=1); ?>
<?php /** @var \Vpn\Portal\Tpl $this */ ?>
<?php /** @var int $maxActiveConfigurations */ ?>
<?php $this->layout('base', ['activeItem' => 'home', 'pageTitle' => $this->t('Home')]); ?>
<?php $this->start('content'); ?>
    <p class="lead"><?= $this->t('Welcome to eduVPN!'); ?></p>

    <p>
        <?= $this->t('To use eduVPN, download the app for your device below!'); ?>
    </p>

    <ul class="apps">
        <li>
            <a target="_blank" class="Windows" href="https://app.eduvpn.org/windows/eduVPNClient_latest.exe"><?= $this->t('Windows'); ?></a>
        </li>

        <li>
            <a target="_blank" class="macOS" href="https://apps.apple.com/app/eduvpn-client/id1317704208"><?= $this->t('macOS'); ?></a>
        </li>

        <li>
            <a target="_blank" class="Android" href="https://play.google.com/store/apps/details?id=nl.eduvpn.app"><?= $this->t('Android'); ?></a>
        </li>

        <li>
            <a target="_blank" class="iOS" href="https://apps.apple.com/app/eduvpn-client/id1292557340"><?= $this->t('iOS'); ?></a>
        </li>

        <li>
            <a target="_blank" class="Linux" href="https://docs.eduvpn.org/client/linux/"><?= $this->t('Linux'); ?></a>
        </li>
    </ul>

    <p>
        <?= $this->t('On the "Account" page you can block access to the VPN in case you lose a device, or no longer use the VPN.'); ?>
    </p>

<?php if (0 !== $maxActiveConfigurations): ?>
    <p>
        <?= $this->t('If you do not want to, or cannot use the official eduVPN apps, you can also manually obtain a VPN configuration and import it in your existing VPN application.'); ?>
    </p>
    <details>
        <summary><?= $this->t('Manual Configuration'); ?></summary>
<?= $this->insert('manualConfiguration'); ?>
    </details>
<?php endif; ?>
<?php $this->stop('content'); ?>
