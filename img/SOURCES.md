# Sources

[https://github.com/eduvpn/artwork](https://github.com/eduvpn/artwork)

# OS Logos

We took the OS logos from Wikipedia and converted them to 128x128 PNG files
using [ImageMagick](https://imagemagick.org/) and 
[GIMP](https://www.gimp.org/).

- [Windows](https://en.wikipedia.org/wiki/Microsoft_Windows)
- [macOS](https://en.wikipedia.org/wiki/MacOS)
- [Android](https://en.wikipedia.org/wiki/Android_(operating_system)
- [iOS](https://en.wikipedia.org/wiki/IOS)
- [Linux](https://en.wikipedia.org/wiki/Linux)
